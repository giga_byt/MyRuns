# CodeIt2 README

## CHANGELOG:
### MainActivity

1. Disabled Swiping 

### FindFragment

1. Caught error when Gender was unselected
2. Fixed 2 references to wrong ViewIDs
3. Fixed redundant casting and instantiation of RadioButton
4. Fixed null-pointer for Prefs by instantiating it

### PartyFragment

1. Fixed references to Calendar
2. Fixed wrong layout choice
3. Added a missing reference to Venue
4. Fixed button reference
