package edu.dartmouth.cs.dartnets.testthis;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.io.ByteArrayOutputStream;

/**
 *
 * Individual tests are defined as any method beginning with 'test'.
 *
 * ActivityInstrumentationTestCase2 allows these tests to run alongside a running
 * copy of the application under inspection. Calling getActivity() will return a
 * handle to this activity (launching it if needed).
 *
 * Instrumented test, which will execute on an Android device.
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ProfileActivityTest extends ActivityInstrumentationTestCase2<ProfileActivity> {


    public ProfileActivityTest() {
        super(ProfileActivity.class);
    }

    /**
     * Test to make sure the image is persisted after screen rotation.
     *
     * Launches the main activity, sets a test bitmap, rotates the screen.
     * Checks to make sure that the bitmap value matches what we set it to.
     */
    public void testImagePersistedAfterRotate() throws InterruptedException {
        // Define a test bitmap
        final Bitmap TEST_BITMAP = BitmapFactory.decodeResource(getActivity().getResources(),
                R.drawable.blue_pushpin);

        // Convert bitmap to byte array
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        TEST_BITMAP.compress(Bitmap.CompressFormat.PNG, 100, bos);
        final byte[] TEST_BITMAP_VALUE = bos.toByteArray();

        final ImageView mImageView = getActivity().findViewById(R.id.imageProfile);
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // Attempts to manipulate the UI must be performed on a UI thread.
                // Calling this outside runOnUiThread() will cause an exception.
                //
                // You could also use @UiThreadTest, but activity lifecycle methods
                // cannot be called if this annotation is used.
                //set the test bitmap to the image view
                mImageView.setImageBitmap(TEST_BITMAP);
            }
        });

        // Suspends the current thread for 1 second. This is no necessary.
        // But you can see the change on your phone.
        Thread.sleep(2000);

        // Information about a particular kind of Intent that is being monitored.
        // It is required to open your phone screen, otherwise the test will be hanging.
        Instrumentation.ActivityMonitor monitor =
                new Instrumentation.ActivityMonitor(ProfileActivity.class.getName(), null, false);
        getInstrumentation().addMonitor(monitor);
        // Rotate the screen
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        getInstrumentation().waitForIdleSync();
        // Updates current activity
        Activity activity = getInstrumentation().waitForMonitor(monitor);

        // Suspends the current thread for 1 second. This is no necessary.
        // But you can see the change on your phone.
        Thread.sleep(2000);

        final ImageView mImageView2 = activity.findViewById(R.id.imageProfile);
        // Get the current bitmap from image view
        Bitmap currentBitMap = ((BitmapDrawable) mImageView2.getDrawable()).getBitmap();

        // Convert bitmap to byte array
        bos = new ByteArrayOutputStream();
        currentBitMap.compress(Bitmap.CompressFormat.PNG, 100, bos);
        byte[] currentBitmapValue = bos.toByteArray();

        // Check if these two bitmaps have the same byte values.
        // If the program executes correctly, they should be the same
        assertTrue(java.util.Arrays.equals(TEST_BITMAP_VALUE, currentBitmapValue));
    }

    /**
     * Test to make sure that value of name is persisted across activity restarts.
     *
     * Launches the main activity, sets a name value, clicks the save button, closes the activity,
     * then relaunches that activity. Checks to make sure that the name value match what we
     * set it to.
     */
    public void testNameValuePersistedBetweenLaunches() throws InterruptedException{
        Activity activity = getActivity();
        final String name = "testStringFoo";

        final Button saveBtn = activity.findViewById(R.id.btnSave);
        final EditText nameField = activity.findViewById(R.id.editName);

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                nameField.requestFocus();
                nameField.setText(name);
                saveBtn.performClick();
            }
        });
        activity.finish();
        setActivity(null);
        activity = this.getActivity();

        final EditText nameField2 = activity.findViewById(R.id.editName);
        String currentName = nameField2.getText().toString();
        assertEquals(name, currentName);
    }

    /**
     * Test to make sure that value of email is persisted across activity restarts.
     *
     * Launches the main activity, sets a email value, clicks the save button, closes the activity,
     * then relaunches that activity. Checks to make sure that the email value match what we
     * set it to.
     */
    public void testEmailValuePersistedBetweenLaunches() {
        Activity activity = getActivity();
        final String firstEmail = "testString@tester.com";

        final Button saveBtn = activity.findViewById(R.id.btnSave);
        final EditText emailField = activity.findViewById(R.id.editEmail);

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                emailField.requestFocus();
                emailField.setText(firstEmail);
                saveBtn.performClick();
            }
        });
        activity.finish();
        setActivity(null);
        activity = this.getActivity();

        final EditText emailField2 = activity.findViewById(R.id.editEmail);
        String currEmail = emailField2.getText().toString();
        assertEquals(firstEmail, currEmail);
    }

    /**
     * Test to make sure that value of phone is persisted across activity restarts.
     *
     * Launches the main activity, sets a phone value, clicks the save button, closes the activity,
     * then relaunches that activity. Checks to make sure that the phone value match what we
     * set it to.
     */
    public void testPhoneValuePersistedBetweenLaunches() {
        Activity activity = getActivity();
        final String phoneNum = "7034013257";

        final Button saveBtn = activity.findViewById(R.id.btnSave);
        final EditText phoneField = activity.findViewById(R.id.editPhone);

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                phoneField.requestFocus();
                phoneField.setText(phoneNum);
                saveBtn.performClick();
            }
        });
        activity.finish();
        setActivity(null);
        activity = this.getActivity();

        final EditText phoneField2 = activity.findViewById(R.id.editPhone);
        String currNum = phoneField2.getText().toString();
        assertEquals(phoneNum, currNum);
    }

    /**
     * Test to make sure that value of gender is persisted across activity restarts.
     *
     * Launches the main activity, sets a gender value, clicks the save button, closes the activity,
     * then relaunches that activity. Checks to make sure that the gender value match what we
     * set it to.
     */
    public void testGenderValuePersistedBetweenLaunches() {
        Activity activity = getActivity();

        final Button saveBtn = activity.findViewById(R.id.btnSave);
        final RadioButton b = activity.findViewById(R.id.radioGenderF);

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                b.requestFocus();
                b.setChecked(true);
                saveBtn.performClick();
            }
        });
        activity.finish();
        setActivity(null);
        activity = this.getActivity();

        final RadioButton b2 = activity.findViewById(R.id.radioGenderF);
        assertEquals(true, b2.isChecked());
    }

    /**
     * Test to make sure that value of class is persisted across activity restarts.
     *
     * Launches the main activity, sets a class value, clicks the save button, closes the activity,
     * then relaunches that activity. Checks to make sure that the class value match what we
     * set it to.
     */
    public void testClassValuePersistedBetweenLaunches() {
        Activity activity = getActivity();
        final String dclass = "2020";

        final Button saveBtn = activity.findViewById(R.id.btnSave);
        final EditText classField = activity.findViewById(R.id.editClass);

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                classField.requestFocus();
                classField.setText(dclass);
                saveBtn.performClick();
            }
        });
        activity.finish();
        setActivity(null);
        activity = this.getActivity();

        final EditText classField2 = activity.findViewById(R.id.editClass);
        String dclass2 = classField2.getText().toString();
        assertEquals(dclass, dclass2);
    }

    /**
     * Test to make sure that value of major is persisted across activity restarts.
     *
     * Launches the main activity, sets a major value, clicks the save button, closes the activity,
     * then relaunches that activity. Checks to make sure that the major value match what we
     * set it to.
     */
    public void testMajorValuePersistedBetweenLaunches() {
        Activity activity = getActivity();
        final String major = "Computer Science";

        final Button saveBtn = activity.findViewById(R.id.btnSave);
        final EditText majorField = activity.findViewById(R.id.editMajor);

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                majorField.requestFocus();
                majorField.setText(major);
                saveBtn.performClick();
            }
        });
        activity.finish();
        setActivity(null);
        activity = this.getActivity();

        final EditText majorField2 = activity.findViewById(R.id.editMajor);
        String major2 = majorField2.getText().toString();
        assertEquals(major, major2);
    }

    /**
     * Test to make sure that image is persisted across activity restarts.
     *
     * Launches the main activity, sets an image, clicks the save button, closes the activity,
     * then relaunches that activity. Checks to make sure that the image matches what we
     * set it to.
     */
    public void testImagePersistedBetweenLaunches(){
        // Define a test bitmap
        final Bitmap TEST_BITMAP = BitmapFactory.decodeResource(getActivity().getResources(),
                R.drawable.blue_pushpin);

        Activity activity = getActivity();
        // Convert bitmap to byte array
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        TEST_BITMAP.compress(Bitmap.CompressFormat.PNG, 100, bos);
        final byte[] TEST_BITMAP_VALUE = bos.toByteArray();

        final ImageView mImageView = activity.findViewById(R.id.imageProfile);
        final Button save = activity.findViewById(R.id.btnSave);
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // Attempts to manipulate the UI must be performed on a UI thread.
                // Calling this outside runOnUiThread() will cause an exception.
                //
                // You could also use @UiThreadTest, but activity lifecycle methods
                // cannot be called if this annotation is used.
                //set the test bitmap to the image view
                mImageView.setImageBitmap(TEST_BITMAP);
                save.performClick();
            }
        });
        activity.finish();
        setActivity(null);
        activity = getActivity();

        final ImageView mImageView2 = activity.findViewById(R.id.imageProfile);
        Bitmap currentBitMap = ((BitmapDrawable) mImageView2.getDrawable()).getBitmap();

        bos = new ByteArrayOutputStream();
        currentBitMap.compress(Bitmap.CompressFormat.PNG, 100, bos);
        byte[] currentBitmapValue = bos.toByteArray();

        assertTrue(java.util.Arrays.equals(TEST_BITMAP_VALUE, currentBitmapValue));
    }


}
