# MyRuns 3

MyRuns3 was the project in which I built the database behind the exercise data. Introduced in this lab
was the history tab and the database structure.

# Installation

No special requirements are necessary; all libraries are already implemented in the build.gradle file.
The application simply needs to be built into an .apk file, and it'll be ready to install and use.

# Testing

There are no tests here, as it requires an emulator and physical handling to run for the edge cases.

# Built With

Basic Android, of course.

The GSON library, from Google, for JSON conversion was also useful.