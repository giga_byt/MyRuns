package com.tang.myRuns;

import android.content.Context;
import android.os.AsyncTask;

/**
 * Created by Tang on 4/24/2018.
 */

public class DeleteDatabaseEntryTask extends AsyncTask<Long, Void, Void> {

    private ExerciseEntryDbHelper datasource;

    DeleteDatabaseEntryTask(Context context){
        super();
        datasource = new ExerciseEntryDbHelper(context);
    }
    @Override
    protected void onPreExecute(){
        datasource.openDb();
    }
    protected Void doInBackground(Long... ids){
        //put the entry into the db

        for(int i = 0; i < ids.length; i++){
            datasource.removeEntry(ids[i]);
        }
        return null;
    }
    protected void onProgressUpdate(Void... params){
        super.onProgressUpdate();
    }

    protected void onPostExecute(Void result){
        datasource.closeDb();
        super.onPostExecute(result);
    }
}
