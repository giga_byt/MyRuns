package com.tang.myRuns;

import android.support.v4.content.AsyncTaskLoader;
import android.content.Context;
import android.support.annotation.Nullable;

import java.util.ArrayList;

/**
 * Created by Tang on 4/24/2018.
 */

public class ExerciseEntryLoader extends AsyncTaskLoader<ArrayList<ExerciseEntry>> {
    private ExerciseEntryDbHelper db;

    ExerciseEntryLoader(Context context){
        super(context);
        db = new ExerciseEntryDbHelper(context);
    }

    @Override
    @Nullable
    public ArrayList<ExerciseEntry> loadInBackground(){
        db.openDb();
        ArrayList<ExerciseEntry> entries = db.fetchEntries();
        db.closeDb();
        return entries;
    }
}
