package com.tang.myRuns;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class HistoryFragment extends Fragment implements LoaderManager.LoaderCallbacks<ArrayList<ExerciseEntry>>{

    protected View mView;
    private Context mContext;
    private ListView list;
    private HistoryListAdapter adapter;
    LoaderManager mLoader;
    private FloatingActionButton syncButton;

    private FirebaseDatabase database;
    private FirebaseAuth mAuth;
    private String userId;

    public HistoryFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
        userId = mAuth.getCurrentUser().getUid();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_history, container, false);
        syncButton = mView.findViewById(R.id.history_sync_button);
        initializeSyncButton();
        adapter = new HistoryListAdapter(mContext);
        list = mView.findViewById(R.id.history_list_view);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent goToActivity = null;
                if(adapter.getData().get(position).getmInputType() == StartFragment.INPUT_TO_ID_MAP.get("Manual")) {
                    goToActivity = new Intent(mContext, ManualActivity.class);
                } else if(adapter.getData().get(position).getmInputType() == StartFragment.INPUT_TO_ID_MAP.get("GPS")){
                    goToActivity = new Intent(mContext, MapActivity.class);
                } else {
                    goToActivity = new Intent(mContext, AutomaticActivity.class);
                }
                goToActivity.putExtra("SENDING_ACTIVITY_ID", "HistoryFragment");
                goToActivity.putExtra("ID", adapter.getData().get(position).getId());
                mContext.startActivity(goToActivity);
            }
        });
        mLoader = getLoaderManager();
        mLoader.initLoader(1, null, this).forceLoad();
        adapter.notifyDataSetChanged();
        return mView;
    }

    private void initializeSyncButton(){
        syncButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isNetworkAvailable()){
                    final Gson gson = new Gson();
                    final DatabaseReference ref = database.getReference().child("data").child(userId);
                    ref.addChildEventListener(new ChildEventListener() {
                        @Override
                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                            Type entryType = new TypeToken<ExerciseEntry>(){}.getType();
                            ExerciseEntry temp = gson.fromJson(dataSnapshot.getValue(String.class), entryType);
                            InsertDatabaseEntryTask run = new InsertDatabaseEntryTask(mContext);
                            run.execute(temp);
                            adapter.notifyDataSetChanged();
                            mLoader.initLoader(1, null, HistoryFragment.this).forceLoad();
                        }

                        @Override
                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {                        }

                        @Override
                        public void onChildRemoved(DataSnapshot dataSnapshot) {

                        }

                        @Override
                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                } else {
                    Toast.makeText(mContext, "Need Internet Access!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) mContext.getSystemService(mContext.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void onAttach(Context context){
        super.onAttach(context);
        mContext = context;
    }

    public void onPause(){
        super.onPause();
    }
    public void onResume(){
        mLoader.initLoader(1, null, this).forceLoad();
        adapter.notifyDataSetChanged();
        super.onResume();
    }

    @NonNull
    @Override
    public Loader<ArrayList<ExerciseEntry>> onCreateLoader(int id, @Nullable Bundle args) {
        if(id == 1){
            return new ExerciseEntryLoader(mContext);
        }
        return null;
    }

    @Override
    public void onLoadFinished(@NonNull Loader<ArrayList<ExerciseEntry>> loader, ArrayList<ExerciseEntry> data) {
        if(loader.getId() == 1){
            adapter.setData(data);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<ArrayList<ExerciseEntry>> loader) {
        if(loader.getId() == 1){
            adapter.notifyDataSetChanged();
        }
    }
}
