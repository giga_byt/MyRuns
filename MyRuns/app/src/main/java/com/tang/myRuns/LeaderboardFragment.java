package com.tang.myRuns;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;

public class LeaderboardFragment extends Fragment implements LoaderManager.LoaderCallbacks<ArrayList<JSONObject>>{

    protected View mView;
    private Context mContext;
    private ListView list;
    private LeaderboardAdapter adapter;
    LoaderManager mLoader;

    public LeaderboardFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_leaderboard, container, false);
        adapter = new LeaderboardAdapter(mContext);
        list = mView.findViewById(R.id.leaderboard_list_view);
        list.setAdapter(adapter);
        mLoader = getLoaderManager();
        mLoader.initLoader(1, null, this).forceLoad();
        adapter.notifyDataSetChanged();
        return mView;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) mContext.getSystemService(mContext.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    public void onPause(){
        super.onPause();
    }
    public void onResume(){
        mLoader.initLoader(1, null, this).forceLoad();
        adapter.notifyDataSetChanged();
        super.onResume();
    }

    @NonNull
    @Override
    public Loader<ArrayList<JSONObject>> onCreateLoader(int id, @Nullable Bundle args) {
        if(id == 1){
            return new LeaderboardLoader(mContext);
        }
        return null;
    }

    @Override
    public void onLoadFinished(@NonNull Loader<ArrayList<JSONObject>> loader, ArrayList<JSONObject> data) {
        if(loader.getId() == 1){
            adapter.setData(data);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<ArrayList<JSONObject>> loader) {
        if(loader.getId() == 1){
            adapter.notifyDataSetChanged();
        }
    }
}

