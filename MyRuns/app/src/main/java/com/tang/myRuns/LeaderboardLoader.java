package com.tang.myRuns;
import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Map;

/**
 * Created by Tang on 5/14/2018.
 */

public class LeaderboardLoader extends AsyncTaskLoader<ArrayList<JSONObject>> {
    private ExerciseEntryDbHelper db;

    public LeaderboardLoader(Context context) {
        super(context);
        db = new ExerciseEntryDbHelper(context);
    }

    @Override
    public ArrayList<JSONObject> loadInBackground() {
        db.openDb();
        ArrayList<ExerciseEntry> entries = db.fetchEntries();
        postEntries(entries);
        ArrayList<JSONObject> data = fetchLeaderboards();
        db.closeDb();
        return data;
    }

    public ArrayList<JSONObject> fetchLeaderboards() {
        final ArrayList<JSONObject> data = new ArrayList<JSONObject>();
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest
                (Request.Method.GET, "http://129.170.212.93:5000/get_exercises", null, new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        JSONArray jsonArray = response;

                        for(int i = 0; i < jsonArray.length(); i++){
                            JSONObject jsonObject;
                            try {
                                jsonObject = jsonArray.getJSONObject(i);
                                data.add(jsonObject);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        if(error.getMessage() != null)
                            Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });

        RequestQueue queue = Volley.newRequestQueue(getContext());
        queue.add(jsonArrayRequest);
        return data;
    }

    public void postEntries(ArrayList<ExerciseEntry> entries){
        for(int i = 0; i < entries.size(); i++){
            final ExerciseEntry entry = entries.get(i);
            if(entry.hasBeenShared() == ExerciseEntry.FALSE){
                JSONObject jsonObject = new JSONObject();
                try {
                    FirebaseAuth mAuth = FirebaseAuth.getInstance();
                    FirebaseUser user = mAuth.getCurrentUser();
                    jsonObject.put("email", user.getEmail());
                    jsonObject.put("activity_type", StartFragment.ID_TO_ACTIVITY_MAP.get(entry.getmActivityType()));
                    jsonObject.put("activity_date", entry.getmDateTime());
                    jsonObject.put("input_type", StartFragment.ID_TO_INPUT_MAP.get(entry.getmInputType()));
                    jsonObject.put("duration", entry.getmDuration());
                    jsonObject.put("distance", (int)entry.getmDistance());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                        (Request.Method.POST,"http://129.170.212.93:5000/upload_exercise", jsonObject, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    if(!response.has("result") || !response.getString("result").equalsIgnoreCase("success")){
                                        entry.setShared(ExerciseEntry.TRUE);
                                        DeleteDatabaseEntryTask run = new DeleteDatabaseEntryTask(getContext());
                                        run.execute(entry.getId());
                                        InsertDatabaseEntryTask walk = new InsertDatabaseEntryTask(getContext());
                                        walk.execute(entry);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                if(error.getMessage() != null)
                                    Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();

                            }
                        }){
                };
                RequestQueue queue = Volley.newRequestQueue(getContext());
                queue.add(jsonObjectRequest);
            }
        }
    }
}
