package com.tang.myRuns;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.HashMap;


public class StartFragment extends Fragment {

    public static final HashMap<String, Integer> INPUT_TO_ID_MAP = new HashMap<>();
    public static final HashMap<String, Integer> ACTIVITY_TO_ID_MAP = new HashMap<>();

    public static final HashMap<Integer, String> ID_TO_INPUT_MAP = new HashMap<>();
    public static final HashMap<Integer, String> ID_TO_ACTIVITY_MAP = new HashMap<>();


    protected View mView;
    private Spinner inputSpinner;
    private Spinner activitySpinner;
    private Context mContext;

    public StartFragment(){}

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_start, container, false);

        initializeSpinners();
        initializeActionButton();

        return mView;
    }

    public void onAttach(Context context){
        super.onAttach(context);
        mContext = context;
    }

    /* helper methods to initialize UI */
    public void initializeActionButton(){
        FloatingActionButton startButton = mView.findViewById(R.id.start_fragment_button);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                switch(inputSpinner.getSelectedItem().toString()){
                    case "GPS":
                        intent = new Intent(mContext, MapActivity.class);
                        intent.putExtra("SENDING_ACTIVITY_ID", "MainActivity");
                        intent.putExtra("ACTIVITY_TYPE", activitySpinner.getSelectedItem().toString());
                        intent.putExtra("INPUT_TYPE", inputSpinner.getSelectedItem().toString());
                        mContext.startActivity(intent);
                        return;
                    case "Manual":
                        intent = new Intent(mContext, ManualActivity.class);
                        intent.putExtra("SENDING_ACTIVITY_ID", "MainActivity");
                        intent.putExtra("ACTIVITY_TYPE", activitySpinner.getSelectedItem().toString());
                        mContext.startActivity(intent);
                        return;
                    case "Automatic":
                        intent = new Intent(mContext, MapActivity.class);
                        intent.putExtra("SENDING_ACTIVITY_ID", "MainActivity");
                        intent.putExtra("INPUT_TYPE", inputSpinner.getSelectedItem().toString());
                        mContext.startActivity(intent);
                }
            }
        });
    }
    public void initializeSpinners(){
        String [] items = new String[]{"Manual", "GPS", "Automatic"};
        String [] activityTypeItems = new String[]{
                "Running",
                "Walking",
                "Standing",
                "Cycling",
                "Hiking",
                "Downhill Skiing",
                "Cross-Country Skiing",
                "Snowboarding",
                "Skating",
                "Swimming",
                "Mountain Biking",
                "Wheelchair",
                "Elliptical",
                "Other"
        };
        for(int i = 0; i < items.length; i++){
            INPUT_TO_ID_MAP.put(items[i], i);
            ID_TO_INPUT_MAP.put(i, items[i]);
        }
        for(int i = 0; i < activityTypeItems.length; i++){
            ACTIVITY_TO_ID_MAP.put(activityTypeItems[i], i);
            ID_TO_ACTIVITY_MAP.put(i, activityTypeItems[i]);
        }

        inputSpinner = mView.findViewById(R.id.spinner_input);
        activitySpinner = mView.findViewById(R.id.spinner_activity);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(mContext,
                android.R.layout.simple_spinner_item, items);
        ArrayAdapter<String> activityAdapter = new ArrayAdapter<>(mContext,
                android.R.layout.simple_spinner_item, activityTypeItems);
        inputSpinner.setAdapter(adapter);
        activitySpinner.setAdapter(activityAdapter);
        inputSpinner.setOnItemSelectedListener(new Spinner.OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id){
                if(inputSpinner.getSelectedItem().toString().equals("Automatic")){
                    activitySpinner.setEnabled(false);
                    activitySpinner.setClickable(false);
                } else {
                    activitySpinner.setEnabled(true);
                    activitySpinner.setClickable(true);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView){
            }
        });
    }
}
