package com.tang.myRuns;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Timer;

public class TrackerService extends Service {
    private NotificationManager notificationManager;
    private Calendar startTime = Calendar.getInstance();
    private static boolean isRunning = false;

    private static int SECOND_INTERVAL = 1;

    public static final int MSG_REGISTER_CLIENT = 0;
    public static final int MSG_UNREGISTER_CLIENT = 1;
    public static final int MSG_INPUT_ACTIVITY_TYPE = 2;

    public static final String CHANNEL_ID = "tracker";

    private ArrayList<LatLng> locationList;
    private ArrayList<Integer> timeList;
    private HashMap<String, Integer> activityHistory = new HashMap<>();
    private String input;
    private String activity;
    private Location firstLoc;

    private final Messenger messenger = new Messenger(new IncomingMessageHandler());
    private ArrayList<Messenger> clients = new ArrayList<>();

    private class IncomingMessageHandler extends Handler {
        @RequiresApi(api = Build.VERSION_CODES.O)
        public void handleMessage(Message msg){
            switch (msg.what) {
                case MSG_REGISTER_CLIENT:
                    clients.add(msg.replyTo);//replyTo is the Messanger, that carrys the Message over.
                    if(firstLoc != null) {
                        updateLocation(firstLoc);
                    }
                    break;
                case MSG_UNREGISTER_CLIENT:
                    clients.remove(msg.replyTo);// each client has a dedicated Messanger to communicae with ther server.
                    break;
                case MSG_INPUT_ACTIVITY_TYPE:
                    input = msg.getData().getString("input");
                    activity = msg.getData().getString("activity");
                    if(activity != null) {
                        if(activityHistory.get(activity) == null){
                            activityHistory.put(activity, 1);
                        } else {
                            activityHistory.put(activity, activityHistory.get(activity) + 1);
                        }

                    }
                    showNotification();
                default:
                    super.handleMessage(msg);
            }
        }
    }

    public IBinder onBind(Intent intent){
        Log.d("#Binder", "isBound");
        return messenger.getBinder();
    }

    public int onStartCommand(Intent intent, int flags, int startId){
        return START_STICKY;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void onCreate(){
        super.onCreate();
        isRunning = true;
        showNotification();
        LocationManager locMan;
        locMan = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        Criteria crit = new Criteria();
        crit.setAccuracy(Criteria.ACCURACY_FINE);
        crit.setPowerRequirement(Criteria.POWER_LOW);
        crit.setAltitudeRequired(true);
        crit.setSpeedRequired(false);
        crit.setCostAllowed(false);

        String provider = locMan.getBestProvider(crit, true);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.d("#Map", "InitializeMap -- Permission has not been granted.");
            return;
        }
        locMan.requestSingleUpdate(provider, new DummyListener(), null);
        Location loc = locMan.getLastKnownLocation(provider);
        Log.d("#Map", "LocLL?");
        locationList = new ArrayList<>();
        timeList = new ArrayList<>();
        updateLocation(loc);
        firstLoc = loc;
        locMan.requestLocationUpdates(provider, SECOND_INTERVAL * 1000, 0, locationListener);
    }

    private void updateLocation(Location loc){
        Log.d("#Map", "called?");
        LatLng currLoc = locToLatLng(loc);
        locationList.add(currLoc);
        timeList.add((int)((Calendar.getInstance().getTimeInMillis() - startTime.getTimeInMillis())/1000));
        sendLocations(loc.getAltitude());
    }

    private final LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            updateLocation(location);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {        }

        @Override
        public void onProviderEnabled(String provider) {        }

        @Override
        public void onProviderDisabled(String provider) {        }
    };

    private LatLng locToLatLng(Location l){
        return new LatLng(l.getLatitude(), l.getLongitude());
    }

    public static boolean isRunning(){
        return isRunning;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void showNotification() {
        NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, "channel name", NotificationManager.IMPORTANCE_DEFAULT);

        Intent intent = new Intent(this, MapActivity.class);
        intent.putExtra("SENDING_ACTIVITY_ID", "TrackerService");
        intent.putExtra("INPUT_TYPE", input);
        if(activity != null) {
            intent.putExtra("ACTIVITY_TYPE", activity);
        } else{
            intent.putExtra("ACTIVITY_TYPE", "Other");
        }
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);// this is the main app page it will show by clicking the notification
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle(this.getString(R.string.notif_title))
                .setContentText(getResources().getString(R.string.notif_text))
                .setSmallIcon(R.drawable.ic_app_icon)
                .setContentIntent(contentIntent);
        Notification notification = notificationBuilder.build();
        notification.flags = notification.flags | Notification.FLAG_ONGOING_EVENT;

        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.createNotificationChannel(notificationChannel);

        notificationManager.notify(0, notification);
    }

    private void sendLocations(double altitude){
        for(int i = 0; i < clients.size(); i++){
            Messenger m  = clients.get(i);
            try{
                Gson gson = new Gson();
                Bundle bundle = new Bundle();
                bundle.putString("locationList_JSON", gson.toJson(locationList));
                bundle.putString("timeList_JSON", gson.toJson(timeList));
                bundle.putDouble("altitude", altitude);
                String ac = "Other";
                for(Map.Entry<String, Integer> entry : activityHistory.entrySet()){
                    int mode = 0;
                    ac = "Other";
                    if(entry.getValue() > mode){
                        ac = entry.getKey();
                    }
                }
                bundle.putString("frequentActivity", ac);
                Message msg = Message.obtain();
                msg.setData(bundle);
                m.send(msg);
            } catch (RemoteException e){
                clients.remove(m);
            }
        }
    }
    public void onDestroy() {
        super.onDestroy();
        notificationManager.cancelAll(); // Cancel the persistent notification.
        isRunning = false;
    }
}
