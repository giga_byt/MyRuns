package com.tang.myRuns;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;


public class ViewPagerAdapter extends FragmentPagerAdapter {
    private ArrayList<Fragment> fragments;

    private static final int START = 1;
    private static final int HISTORY = 2;

    private static final String START_TAB_TITLE = "START";
    private static final String HISTORY_TAB_TITLE = "HISTORY";

    public ViewPagerAdapter(FragmentManager fm, ArrayList<Fragment> fragments){
        super(fm);
        this.fragments = fragments;
    }

    // Return the Fragment associated with a specified position.
    public Fragment getItem(int pos){
        return fragments.get(pos);
    }
    // Return the number of views available
    public int getCount(){
        return fragments.size();
    }

    // This method may be called by the ViewPager to obtain a title string
    // to describe the specified page
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case START:
                return START_TAB_TITLE;
            case HISTORY:
                return HISTORY_TAB_TITLE;
            default:
                break;
        }
        return null;
    }
}
